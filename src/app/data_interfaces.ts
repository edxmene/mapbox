import { Marker } from 'mapbox-gl';

export interface dataModel {
  category: string;
  location_type: string;
  location: {
    latitude: string;
    street: {
      id: number;
      name: string;
    };
    longitude: string;
  };
  context: string;
  outcome_status: null;
  persistent_id: string;
  id: number;
  location_subtype: string;
  month: string;
}

export function marker_mapbox() {
  return new Marker().setLngLat([52.653555, -1.131454]);
}
