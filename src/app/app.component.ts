import { HttpClient } from '@angular/common/http';
import {
  AfterViewInit,
  Component,
  ElementRef,
  OnInit,
  ViewChild,
} from '@angular/core';
// import mapboxgl, { Map, Marker, Popup } from 'mapbox-gl';
import {
  Circle,
  circle,
  LatLng,
  layerGroup,
  LayerGroup,
  Map,
  Marker,
  marker,
  popup,
  tileLayer,
} from 'leaflet';
import { dataModel } from './data_interfaces';
import { distance } from '@turf/turf';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements AfterViewInit, OnInit {
  @ViewChild('mapDiv') mapDivElement!: ElementRef;
  title = 'map-box';
  token_mapbox: string =
    'pk.eyJ1IjoiZWR4b24tbWVuZXNlcyIsImEiOiJjbDE4aWlpc2Mwb3R4M2ludzF4ajF0Z2FzIn0.JhsGN6UFiK1DGDix6qRMig';
  map!: any;
  template_popUp = `
  <div style="text-align:center">
    <h3>National Space Centre</h3>
    <img width="140" height="150" src="../assets/National_Space_Centre_Leicester.jpg" />
    <p>The National Space Centre is a museum and educational resource covering the fields of space science and astronomy, along with a space research programme in partnership with the University of Leicester.</p>
  </div>
  `;
  circularAreaLatitud!: string;
  circularAreaLongitud!: string;
  circularAreaRadius!: string;
  data_report: any[] = [];

  constructor(private http: HttpClient) {}
  marker!: any;
  circle!: any;
  circleRadius: number = 500;
  markers = layerGroup();

  ngOnInit(): void {
    this.marker = new Marker([0, 0]);
    this.circle = new Circle([0, 0]);

    this.fetchingData();
  }

  ngAfterViewInit(): void {
    this.map = new Map('map').setView([52.639, -1.13], 15.5); //[52.639, -1.13]
    tileLayer(
      'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}',
      {
        attribution:
          'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: this.token_mapbox,
      }
    ).addTo(this.map);

    this.marker = new Marker([52.653555, -1.131454]);
    this.marker.addTo(this.map);
    this.marker.bindPopup(this.template_popUp);
    marker([52.653555, -1.131454]).addTo(this.map);

    // mapboxgl.accessToken = this.token_mapbox;

    // this.map = new Map({
    //   container: this.mapDivElement.nativeElement, // container ID
    //   style: 'mapbox://styles/mapbox/streets-v11', // style URL
    //   center: [-1.13, 52.639], // starting position [lng, lat]
    //   zoom: 15, // starting zoom
    // });

    // const popup = new Popup().setHTML(this.template_popUp);

    // new Marker({ color: 'red' })
    //   .setLngLat([-1.13, 52.639])
    //   .setPopup(popup)
    //   .addTo(this.map);

    this.map.on('click', (e: any) => {
      this.marker.remove();
      this.circle.remove();
      this.marker = new Marker([e.latlng.lat, e.latlng.lng], {}).addTo(
        this.map
      );
      // marker_mapbox().addTo(this.map);
      this.circle = new Circle([e.latlng.lat, e.latlng.lng], {
        radius: this.circleRadius,
      }).addTo(this.map);

      this.createrMarkers(e.latlng);
    });
  }

  onFetchingData() {
    this.fetchingData();
  }

  private fetchingData() {
    this.http
      .get(
        'https://data.police.uk/api/crimes-street/all-crime?lat=52.629729&lng=-1.131592&date=2019-10'
      )
      .subscribe((data: any) => {
        // this.createrMarkers(data);
        this.data_report = data;
        // console.log(this.data_report);
      });
  }

  private createrMarkers(endCoordinate: any) {
    this.markers.clearLayers();
    this.data_report.forEach((data: dataModel) => {
      const startCoordinate = new LatLng(
        +data.location.latitude,
        +data.location.longitude
      );
      const distance = startCoordinate.distanceTo(endCoordinate);
      if (distance <= this.circleRadius) {
        const marker_element = new Marker([
          +data.location.latitude,
          +data.location.longitude,
        ]);
        this.markers.addLayer(marker_element);
      }
    });
    this.markers.addTo(this.map);
  }

  onCreateCircleArea() {
    // const lat = +this.circularAreaLatitud;
    // const lon = +this.circularAreaLongitud;
    // circle([lon, lat], {
    //   radius: +this.circularAreaRadius,
    // }).addTo(this.map);
  }

  // onRoute() {
  //   Rou;
  // }
}

// this.map.on('click', (e: any) => {
//   this.marker.remove();
//   this.circle.remove();
//     this.marker = new Marker([e.latlng.lat, e.latlng.lng], {}).addTo(
//       this.map
//     );
//     // marker_mapbox().addTo(this.map);
//     this.circle = new Circle([e.latlng.lat, e.latlng.lng], {
//       radius: this.circleRadius,
//     }).addTo(this.map);

//     this.createrMarkers(e.latlng);
// });

// private createrMarkers(endCoordinate: any) {
//     this.markers = [];
//     this.data_report.forEach((data: dataModel) => {
//       const startCoordinate = new LatLng(
//         +data.location.latitude,
//         +data.location.longitude
//       );
//       const distance = startCoordinate.distanceTo(endCoordinate);
//       if (distance <= this.circleRadius) {
//         const marker_element = new Marker([
//           +data.location.latitude,
//           +data.location.longitude,
//         ]);
//         this.markers.addLayer(marker_element);
//       }
//     });
//     this.markers.addTo(this.map);
//   }

// this.map.on('click', (e: any) => {
//   this.marker.remove();
//   this.marker = new Marker()
//     .setLngLat([e.lngLat.lng, e.lngLat.lat])
//     .addTo(this.map);

//   //   this.circle = new Circle([e.latlng.lat, e.latlng.lng], {
//   //     radius: this.circleRadius,
//   //   }).addTo(this.map);

//   this.createrMarkers([e.lngLat.lng, e.lngLat.lat]);
// });

// private createrMarkers(endCoordinate: any) {
//     const geojson: any = [];
//     this.markers.forEach((markerArray: any) => {
//       markerArray.remove();
//     });
//     this.data_report.forEach((data: dataModel) => {
//       const startCoordinate = [
//         +data.location.longitude,
//         +data.location.latitude,
//       ];
//       const distance_: any = distance(startCoordinate, endCoordinate, {
//         units: 'kilometers',
//       });

//       if (distance_ <= this.circleRadius / 1000) {
//         geojson.push({
//           type: 'Feature',
//           geometry: {
//             type: 'Point',
//             coordinates: [+data.location.longitude, +data.location.latitude],
//           },
//         });
//         // const marker_element = new Marker().setLngLat([
//         //   +data.location.longitude,
//         //   +data.location.latitude,
//         // ]);
//       }
//     });
//   }
